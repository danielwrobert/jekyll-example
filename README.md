# Example project from Tuts+ Course "Building Static Websites with Jekyll"

Course Title: Building Static Websites with Jekyll

Instructor: Guy Routledge

Available on Tuts+ July, 2015

Course URL: https://webdesign.tutsplus.com/courses/building-static-websites-with-jekyll

Course GitHub Repository: https://github.com/tutsplus/building-static-websites-with-jekyll
